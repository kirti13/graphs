#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  1 19:07:33 2022

@author: sc19ks
"""

import time
from alphaGraph import graph     # importing graphs
from largeGraph import G
start=time.time()
graph=graph
graph0=G
graph1 = {
  'A' : ['B','E'],
  'B' : ['E','D','C','A'],
  'E' : ['D','B','A'],
  'D' : ['C','B','E'],
  'C' : ['B','D']
}

graph2 = {
  'A' : ['B','C','D'],
  'B' : ['D','A'],
  'C' : ['E','A'],
  'D' : ['F','B','A'],
  'E' : ['C'],
  'F' : ['D']
}

graph3 = {
  's' : ['u','v','w'],
  'u' : ['x','y','v','s'],
  'v' : ['y','w','s','u'],
  'w' : ['y','z','s','v'],
  'y' : ['x','z','u','v','w'],
  'x' : ['y','u'],
  'z' : ['y','w']
}


max=-1
label={}

def lexigraphicLabel(label): # to get the value with the largest label
    global max,v
    max="-1"
    for x in label:
        c=label[x][0]
        c=str(c)
        if(c>max):
            max=c
            v=label[x][1]
    return v
               

def lexBFS(graph):
    sigma=[]
    
        
    l=len(graph) # length of the graph
    
    for y in graph:
        
        label[y]=[0,y]
            
    for i in reversed(range(1,l+1)):
        
        u=lexigraphicLabel(label) # getting the value with the largest label
        sigma.append(u)
        label.pop(u)
        for z in graph[u]:
            if z not in sigma:
                
                s=label[z][0]
                s=str(s)+str(i)   # appending to the label
                label[z][0]=int(s) # assigning new label
           
    print("The lexicographical BFS order is:")
    print(sigma)
        
    
end=time.time()-start # runtime before the function
   
t0=time.time()
lexBFS(graph) 
t01=time.time()-t0  # runtime of the function
t01=t01+end # total runtime
print("\ntime in seconds for 26 nodes",t01) 


t0=time.time()        
lexBFS(graph0)   
t=time.time()-t0
t=t+end
print("\ntime in seconds for 900 nodes",t)   

      
   
