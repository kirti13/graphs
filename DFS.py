#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 16 13:38:37 2022

@author: sc19ks
"""
from alphaGraph import graph  # importing graphs
from largeGraph import G
import time
start=time.time()
graph2 = {
  'A' : ['B','C'],
  'B' : ['D', 'E','A'],
  'C' : ['F','A'],
  'D' : ['B'],
  'E' : ['B'],
  'F' : ['C']
}
graph=graph
graph1=G
visited=set()
# recursive 
def dfs(graph, node, visited):
    if node not in visited:
        visited.add(node)
        print(node, end=" ")
        for next in graph[node]:   # checking if not visited
            dfs(graph, next, visited)



print("Depth-first search")
end=time.time()-start # runtime before the function

t0=time.time()
dfs(graph, 'A',visited)
t01=time.time()-t0      # runtime of the function
t01=t01+end # total runtime
print("\ntime in seconds for 26 nodes",t01)


t0=time.time()
dfs(graph1, 0, visited)
t=time.time()-t0              # runtime
t=t+end
print("\ntime in seconds for 900 nodes",t)

