#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar  3 18:05:54 2022

@author: sc19ks
"""
from alphaGraph import graph   # importing graphs
from largeGraph import G
import time
start=time.time()
graph=graph
graph0=G
graph1 = {
  'A' : ['B','E'],
  'B' : ['E','D','C','A'],
  'E' : ['D','B','A'],
  'D' : ['C','B','E'],
  'C' : ['B','D']
}

graph2 = {
  'A' : ['B','C','D'],
  'B' : ['D','A'],
  'C' : ['E','A'],
  'D' : ['F','B','A'],
  'E' : ['C'],
  'F' : ['D']
}

graph3 = {
  's' : ['u','v','w'],
  'u' : ['x','y','v','s'],
  'v' : ['y','w','s','u'],
  'w' : ['y','z','s','v'],
  'y' : ['x','z','u','v','w'],
  'x' : ['y','u'],
  'z' : ['y','w']
}

label={}
max=-1

def lexigraphicLabel(label):  # to find the largest label
    global max,v
    max="-1"
    for x in label:
        c=label[x][0]
        c=str(c)
        if(c>max):
            max=str(label[x][0])
            v=label[x][1]
    return v
        

def lexDFS(graph):
    sigma=[]
    l=len(graph)
    
    for y in graph:
        
        label[y]=[0,y] # all labels to 0
      
    for i in range(1,l+1):
               
        u=lexigraphicLabel(label)
        sigma.append(u)
        label.pop(u)
        for z in graph[u]:
            if z not in sigma:
                s=label[z][0]
                s=str(i)+str(s) # prepending to label
                label[z][0]=int(s) #assinging new label
   
    print("The lexicographical DFS order is:")
    print(sigma)
        
    
end=time.time()-start  # runtime before the function

t0=time.time()
lexDFS(graph)      
t01=time.time()-t0 # function runtime
t01=t01+end # total runtime
print("\ntime in seconds for 26 nodes",t01)  

t0=time.time()        
lexDFS(graph0)    
t=time.time()-t0  # runtime
t=t+end
print("\ntime in seconds for 900 nodes",t)  
 

  