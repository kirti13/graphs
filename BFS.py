#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 16 13:14:17 2022

@author: sc19ks
"""
from alphaGraph import graph # importing graphs
from largeGraph import G
import time
start=time.time()
graph2 = {
  'A' : ['B','C'],
  'B' : ['D', 'E','A'],
  'C' : ['F','A'],
  'D' : ['B'],
  'E' : ['B'],
  'F' : ['C']
}
graph1=G
graph=graph

visited = [] # List for visited nodes.
queue = []     #Initialize a queue


def bfs(visited, graph, node): #function for BFS
  visited.append(node)
  queue.append(node)

  while queue:          # Creating loop to visit each node
    v = queue.pop(0)
    print(v, end = " ")

    for neighbour in graph[v]: # checking neighbour
      if neighbour not in visited:
        visited.append(neighbour)
        queue.append(neighbour)


print("Breadth-First Search")
end=time.time()-start # runtime before the function

t0=time.time()
bfs(visited, graph, 'A')
t01=time.time()-t0 # runtime of the function
t01=t01+end # toal runtime
print("\ntime in seconds for 26 nodes",t01)


t0=time.time()
bfs(visited, graph1, 0)
t=time.time()-t0 # runtime
t=t+end
print("\ntime in seconds for 900 nodes",t)
