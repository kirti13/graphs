#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 12 19:22:27 2022

@author: sc19ks
"""
from itertools import combinations
import random
from collections import defaultdict

lst={}

def directedGraph(n):
    
    V=set()
    for v in range(n): # adding vertices
        V.add(str(v))
        
    E=defaultdict(list)
    for combination in combinations(V, 2): # making vertices pair
        a=random.random()
        if a<0.4:
            w=(random.randint(1, 20)) # choosing random pair
            x,y=combination
            E[x].append((y,w)) # adding edge with weight

    return E
    
        
def h(n): # heuristic function
    for i in range(n):
        a=random.randint(1,20)
        i=str(i)
        lst[i]=a
    
def Graph():
    n=900
    G=directedGraph(n)
    h(n)
    return G

G=Graph()

