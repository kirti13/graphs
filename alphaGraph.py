#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  4 17:37:37 2022

@author: sc19ks
"""


from collections import defaultdict

dic=defaultdict(list)

def add(node,adj_node):   # adding edges 
    dic[node].append(adj_node)
    dic[adj_node].append(node)
    


def Graph():
    add('A','B')
    add('A','C')
    add('D','B')
    add('E','B')
    add('C','F')
    add('G','F')
    add('H','F')
    add('I','F')
    add('E','J')
    add('I','J')
    add('M','D')
    add('G','K')
    add('L','K')
    add('H','L')
    add('N','J')
    add('M','O')
    add('N','O')
    add('T','O')
    add('N','S')
    add('L','P')
    add('Q','K')
    add('T','U')
    add('T','V')
    add('S','X')
    add('S','W')
    add('P','R')
    add('Q','R')
    add('Y','R')
    add('Z','R')
    return dic

     
graph=Graph()
