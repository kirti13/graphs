#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  4 19:45:37 2022

@author: sc19ks
"""

import matplotlib.pyplot as plt       # importing runtimes from the algorithms
from BFS import t,t01
t1=t
t11=t01

from DFS import t,t01
t2=t
t22=t01
from LexBFS import t,t01
t3=t
t33=t01
from LexDFS import t,t01
t4=t
t44=t01

from A_star import t,t01
t5=t
t6=t01



def line_plot(numbers,label):  # creating line graphs
    plt.plot(label,numbers)
    plt.title('Time taken by each algorithm for 900 nodes')
    plt.ylabel('time taken')
    plt.xlabel('algorithm')
    plt.show()

def line_plot2(numbers,label):
    plt.plot(label,numbers)
    plt.title('Time taken by A_star algorithm')
    plt.ylabel('time taken')
    plt.xlabel('Number of nodes')
    plt.show()
    
def line_plot3(numbers,label):
    plt.plot(label,numbers)
    plt.title('Time taken by each algorithm for 26 nodes')
    plt.ylabel('time taken')
    plt.xlabel('algorithm')
    plt.show()

    
if __name__ == '__main__':
    numbers = [t1,t2,t3,t4]
    label = ['BFS', 'DFS', 'LexBFS', 'LexDFS']  
    line_plot(numbers,label)
    numbers = [t11,t22,t33,t44]
    label = ['BFS', 'DFS', 'LexBFS', 'LexDFS']  
    line_plot3(numbers,label)
    num= [t5,t6]
    label2=['26','900'] 
    line_plot2(num,label2)

