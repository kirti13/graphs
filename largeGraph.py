#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 14 01:35:56 2022

@author: sc19ks
"""

from itertools import combinations
import random
from collections import defaultdict

lst={}

def LargeGraph(n):
    
    V=set()
    for v in range(n): # adding vertices
        V.add(v)
        
    E=defaultdict(list)
    for combination in combinations(V, 2): # pairing vertices
        x,y=combination
        a=random.random()
        if a<0.4:
            E[x].append(y)   # adding edge
            E[y].append(x)
         
    return E
    

def Graph():
    n=900
    G=LargeGraph(n) 
    return G


G=Graph()
