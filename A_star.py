#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  1 18:29:32 2022

@author: sc19ks
"""
from directedAlphaGraph import graph,dic2    # importing graphs and heuristic
from largeDiGraph import lst,G
import time
start=time.time()
graph0=G
graph=graph
graph1 = {
    'A': [('B', 1), ('C', 3), ('D', 7)],
    'B': [('D', 5)],
    'C': [('D', 12)]
}

graph2 = {
           'A': [('B',3), ('C',5)],
           'B': [('D',2)],
           'C': [('E',3)],
           'D': [('E',1)],
           
         }

    # This is heuristic function for graph1 and graph2
def h1(n):
        heuristic = {
            'A': 5,
            'B': 2,
            'C': 3,
            'D': 1,
            'E': 0
        }
 
        return heuristic[n]
    
def h(n):  # This is heuristic function 
    if n in "ABCDEFGHIJKLMNOPQRSTUVWXYZ":
        c=dic2[n]
    else:
        c=lst[n]
    return c

    
def reconstruct_path(path,current,start): # printing the final path
    new_path=[]
    while path[current] != current:
        new_path.append(current)
        current = path[current]
    new_path.append(start)
    new_path.reverse()
    return new_path
        

def a_star(graph,start, target):
        G = {}
        G[start] = 0
        path = {}
        path[start] = start
        
        open_lst = set([start])
        closed_lst = set([])
 
        while len(open_lst) > 0:
            current_node = None
 
            # it will find a node with the lowest value of f() -
            for node in open_lst:
                f_v = G[node] + h(node)
                
                if current_node != None:
                    f_current_node= G[current_node] + h(current_node)
               
                if current_node == None or f_v < f_current_node:
                    current_node = node;
                    
           
            if current_node == target:
                new_path = reconstruct_path(path, current_node, start)
                
                return new_path
 
            # for all the neighbors of the current node do
            for (next_node, weight) in graph[current_node]:
           
                if next_node not in open_lst:# and next_node not in closed_lst:
                    open_lst.add(next_node)
                    path[next_node] = current_node
                    G[next_node] = G[current_node] + weight
                    
 
                # otherwise, check if it's quicker to first visit current node, than next node

                else:
                    if G[next_node] > G[current_node] + weight:
                        G[next_node] = G[current_node] + weight
                        path[next_node] = current_node
                        
 
            
            open_lst.remove(current_node)
            closed_lst.add(current_node)
            
        return None

end=time.time()-start # runtime before the function

t0=time.time() #runtime of the function
c=a_star(graph,'A', 'Z')
if(c==None):
    print("Path does not exists")
else:
    print("Path found: ",c)
t=time.time()-t0
t=t+end # total runtime
print("\ntime in seconds",t)   

t0=time.time()
c=a_star(graph0,'80', '899')
if(c==None):
    print("Path does not exists")
else:
    print("Path found: ",c)
t01=time.time()-t0
t01=t01+end # total runtime
print("\ntime in seconds",t01)   
