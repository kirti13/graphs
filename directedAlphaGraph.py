#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 11 13:39:21 2022

@author: sc19ks
"""

from collections import defaultdict
import random

dic=defaultdict(list)
dic2={}

def add(node,adj_node,weight):
    dic[node].append((adj_node,weight)) # adding edge with weight

Lst="ABCDEFGHIJKLMNOPQRSTUVWXYZ"    
def h(Lst): # heuristic value
    for i in Lst:
        a=random.randint(1,20) # random value
        dic2[i]=a
        
        

def Graph():
    add('A','B',5)
    add('A','C',3)
    add('B','D',2)
    add('B','E',1)
    add('C','F',7)
    add('F','G',5)
    add('F','H',3)
    add('F','I',6)
    add('E','J',8)
    add('I','J',4)
    add('D','M',9)
    add('G','K',2)
    add('K','L',7)
    add('H','L',1)
    add('J','N',4)
    add('M','O',2)
    add('N','O',5)
    add('O','T',6)
    add('N','S',2)
    add('L','P',8)
    add('K','Q',6)
    add('T','U',9)
    add('T','V',3)
    add('S','X',2)
    add('S','W',2)
    add('P','R',1)
    add('Q','R',3)
    add('R','Y',0)
    add('R','Z',0)
    return dic

     
graph=Graph()
h(Lst)


